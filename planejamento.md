# Cinema API

As issues abertas podem ser encontradas no [Jira](https://lucas3329a.atlassian.net/jira/software/projects/CA/boards/3)!

# /movies - Requisitos funcionais

#### Preparativos de ambiente

• Postman

### CT 01 - Não deve ser possível cadastrar filme com nome repetido.

#### Resultado esperado

Não é possível cadastrar filme com nome repetido.

### CT 02 - O sistema é capaz de criar um filme e atribuir um ID único a ele.

#### Resultado esperado

É possível criar um filme e cada filme possui um ID único.

### CT 03 - O sistema retorna uma lista de todos os filmes cadastrados com detalhes ao realizar uma solicitação GET para /movies.

#### Resultado esperado

A lista de todos os filmes é exibida.

### CT 04 - O sistema verifica a existência do filme e retorna seus detalhes ao realizar uma solicitação GET para /movies/{id}.

#### Resultado esperado

Caso o filme exista, retorna seus detalhes corretamente.

### CT 05 - Ao tentar listar filme informando um ID que não exista, o sistema deve retornar uma mensagem de erro com status 404 Not Found.

#### Resultado esperado

O sistema retorna uma mensagem de erro com status code 404.

### CT 06 - Realizando uma solicitação PUT para /movies/{id} o sistema atualiza os dados de um filme existente caso todas as validações passem. Deve retornar uma resposta de sucesso e os detalhes atualizados do filme.

#### Resultado esperado 

Filme é alterado com sucesso.

### CT 07 - Enviando uma solicitação DELETE para /movies/{id} o sistema retorna uma resposta de sucesso com o status code 204 No Content após exclusão do filme.

#### Resultado esperado

Filme é excluído e uma resposta de sucesso com status code 204 pode ser visualizada.

# /movies - Requisitos não funcionais

### CT 08 - A API deve ser capaz de processar pelo menos 100 solicitações de criação de filmes por segundo.

#### Resultado esperado

A API é capaz de processar pelo menos 100 solicitações de criação de filmes por segundo.

### CT 09 - O tempo médio de resposta para a criação de um novo filme não deve exceder 200 milissegundos.

#### Resultado esperado

O tempo médio para criar um novo filme é inferior ao tempo estipulado de 200ms.

### CT 10 - A API deve ser capaz de responder a solicitações GET de listagem de filmes em menos de 100 milissegundos.

#### Resultado esperado

Solicitações GET para listar todos os filmes são concluídas em menos de 100 milissegundos.

### CT 11 - A lista de filmes deve ser paginada, com no máximo 20 filmes por página.

#### Resultado esperado

Apenas são exibidos 20 filmes por página.

### CT 12 - A API deve ser capaz de responder a solicitações GET de detalhes de um filme em menos de 50 milissegundos.

#### Resultado esperado

Solicitações GET para listar um filme específico são concluídas em menos de 100 milissegundos.

### CT 13 - A API deve ser capaz de processar pelo menos 50 solicitações de atualização de filmes por segundo.

#### Resultado esperado

A API é capaz de processar pelo menos 50 solicitações de atualização de filmes por segundo.

### CT 14 - O tempo médio de resposta para a atualização dos detalhes de um filme não deve exceder 300 milissegundos.

#### Resultado esperado

O tempo médio para atualizar um filme é inferior ao tempo estipulado de 300ms.

### CT 15 - A API deve ser capaz de processar pelo menos 30 solicitações de exclusão de filmes por segundo.

#### Resultado esperado

A API é capaz de processar pelo menos 30 solicitações de exclusão de filmes por segundo.

### CT 16 - O tempo médio de resposta para a exclusão de um filme não deve exceder 400 milissegundos.

#### Resultado esperado

O tempo médio para excluir um filme é inferior ao tempo estipulado de 400ms.

# /tickets - Requisitos funcionais

### CT 17 - Ao enviar uma solicitação POST para /tickets o sistema deve validar se o valor em movieId corresponde a um existente no banco de dados dos filmes.

#### Resultado esperado

A criação de ticket somente é realizada caso o Id do filme exista previamente.

### CT 18 - Todos os campos são obrigatórios e devem ser validados pelo sistema ao efetuar um POST em /tickets.

#### Resultado esperado

Os campos são obrigatórios e validados.

### CT 19 - O número do assento deve ser válido somente se estiver dentro do intervalo de 0 a 99.

#### Resultado esperado

O número do assento é inválido caso não esteja entre o intervalo citado.

### CT 20 - O preço do ingresso deve ser válido somente se estiver dentro do intervalo de 0 a 60.

#### Resultado esperado

O preço do ingresso é inválido caso não esteja entre o intervalo citado.

### CT 21 - O sistema é capaz de criar uma reserva de ingresso com os detalhes fornecidos e atribui a ela um ID único.

#### Resultado esperado

O sistema cria uma reserva de ingresso com ID único.

### CT 22 - É retornada uma resposta de sucesso com o status 201 Created, incluindo o ID da reserva de ingresso pelo sistema.

#### Resultado esperado

É possível visualizar uma resposta de sucesso com status 201 Created e o ID da reserva de ingresso.

# /tickets - Requisitos não funcionais

### CT 23 - A API deve ser capaz de processar pelo menos 50 solicitações de reserva de ingressos por segundo.

#### Resultado esperado

A API é capaz de processar pelo menos 50 solicitações de reserva de ingressos por segundo.

### CT 24 - O tempo médio de resposta para a reserva de um ingresso não deve exceder 300 milissegundos.

#### Resultado esperado

O tempo de resposta para reservar um ingresso não excede o tempo estipulado de 300ms.







