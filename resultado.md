# Cinema API

As issues abertas podem ser encontradas no [Jira](https://lucas3329a.atlassian.net/jira/software/projects/CA/boards/3)!

# movies - Requisitos funcionais

#### CT 01 - FAILED ❌

#### CT 02 - PASSED ✔️

#### CT 03 - PASSED ✔️

#### CT 04 - PASSED ✔️

#### CT 05 - PASSED ✔️

#### CT 06 - FAILED ❌

#### CT 07 - FAILED ❌

# movies - Requisitos não funcionais

#### CT 08 - PASSED ✔️

#### CT 09 - PASSED ✔️

#### CT 10 - PASSED ✔️

#### CT 11 - FAILED ❌

#### CT 12 - FAILED ❌

#### CT 13 - PASSED ✔️

#### CT 14 - PASSED ✔️

#### CT 15 - FAILED ❌

#### CT 16 - PASSED ✔️

# tickets - Requisitos funcionais

#### CT 17 - FAILED ❌

#### CT 18 - FAILED ❌

#### CT 19 - PASSED ✔️

#### CT 20 - PASSED ✔️

#### CT 21 - PASSED ✔️

#### CT 22 - PASSED ✔️

# tickets - Requisitos não funcionais

#### CT 23 - PASSED ✔️

#### CT 24 - PASSED ✔️